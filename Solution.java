package com.javarush.task.task19.task1916;

import java.io.*;
import java.util.*;

/*

В оригинальном задании не было учтено, что новые строки могут быть добавлены в самом начале второго файла.



Считать с консоли 2 имени файла - file1, file2.
Файлы содержат строки, file2 является обновленной версией file1, часть строк совпадают.
Нужно создать объединенную версию строк, записать их в список lines.
Операции ADDED и REMOVED не могут идти подряд, они всегда разделены SAME.
Пустые строки даны в примере для наглядности.
В оригинальном и редактируемом файлах пустых строк нет!

Пример 1:
оригинальный    редактированный    общий
file1:          file2:             результат:(lines)

строка1         строка1            SAME строка1
строка2                            REMOVED строка2
строка3         строка3            SAME строка3
строка4                            REMOVED строка4
строка5         строка5            SAME строка5
                строка0            ADDED строка0
строка1         строка1            SAME строка1
строка2                            REMOVED строка2
строка3         строка3            SAME строка3
                строка4            ADDED строка4
строка5         строка5            SAME строка5
строка0                            REMOVED строка0

Пример 2:
оригинальный    редактированный    общий
file1:          file2:             результат:(lines)

строка1         строка1            SAME строка1
                строка0            ADDED строка0

Пустые строки в примере означают, что этой строки нет в определенном файле.

*/

public class Solution {
    public static List<LineItem> lines = new ArrayList<LineItem>();

    //*********************************** MAIN **********************************************************
    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        String f1 = bufferedReader.readLine();
        String f2 = bufferedReader.readLine();

        bufferedReader.close();

// По одному списку на файл
        ArrayList<String> list1 = new ArrayList<>();
        ArrayList<String> list2 = new ArrayList<>();


        FileReader fileReader = new FileReader(f1);
        FileReader fileReader2 = new FileReader(f2);

        BufferedReader bufferedReader1 = new BufferedReader(fileReader);
        BufferedReader bufferedReader2 = new BufferedReader(fileReader2);

        String inpString;

        while ((inpString = bufferedReader1.readLine()) != null) list1.add(inpString);

        while ((inpString = bufferedReader2.readLine()) != null) list2.add(inpString);

        bufferedReader1.close();
        bufferedReader2.close();

        lines = filesCompare(list1, list2);
        System.out.println(lines);

    }

//********************************** ENUM *****************************************************
    public static enum Type {
        ADDED,        //добавлена новая строка
        REMOVED,      //удалена строка
        SAME          //без изменений
    }

//********************************** КЛАСС ТИП\СТРОКА *****************************************
    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }

        @Override
        public String toString() {
            return type + " " + line;
        }
    }


/*
        Метод принимает 2 списка строк из файлов.
        Сравнивает их построчно и возвращает список пар ТИП\СТРОКА

*/
    public static ArrayList<LineItem> filesCompare(ArrayList<String> aList1, ArrayList<String> aList2) {

        ArrayList<LineItem> result = new ArrayList<>();
        ArrayList<String> arrayList1;
        ArrayList<String> arrayList2;

        arrayList1 = (ArrayList<String>) aList1.clone();
        arrayList2 = (ArrayList<String>) aList2.clone();
//************************** СЧЕТЧИКИ ****************************
        int listOnePoint = 0;
        int listTwoPoint = 0;
// Пока не переберутся оба списка, цикл не закончится
        while ((listOnePoint < arrayList1.size()) || (listTwoPoint < arrayList2.size())) {
// Если Первый список не закончился, а второй уже закончился
            if ((listOnePoint < arrayList1.size()) && (listTwoPoint >= arrayList2.size())) {

                for (int i = listOnePoint; i < arrayList1.size(); i++) {
                    result.add(new LineItem(Type.REMOVED, arrayList1.get(i)));
                }
                break;

            }
// Если Второй список не закончился, а первый уже закончился
            if ((listOnePoint >= arrayList1.size()) && (listTwoPoint < arrayList2.size())) {

                for (int i = listTwoPoint; i < arrayList2.size(); i++) {
                    result.add(new LineItem(Type.ADDED, arrayList2.get(i)));
                }
                break;
            }

            String fromList1 = arrayList1.get(listOnePoint);
            String fromList2 = arrayList2.get(listTwoPoint);
// Если Строки в списках равны
            if (fromList1.equals(fromList2)) {
                result.add(new LineItem(Type.SAME, fromList1));
                listOnePoint++;
                listTwoPoint++;
                continue;
            }
/*
Если строки не были равны, то нам надо поочередно перебирать списки
Текущее значение listOnePoint списка один по очереди сравнивается с оставшимися эл-тами второго списка
Текущее значение listTwoPoint списка два по очереди сравнивается с оставшимися эл-тами первого списка

 */
            int coubter1 = listOnePoint + 1;
            int coubter2 = listTwoPoint + 1;
//*************************************************************************************
            while (true) {

                if (coubter2 < arrayList2.size()) {
/*Если первым было найдено совпадение listOnePoint со вторым списком,
  то значение второго списка помечается , как ДОБАВЛЕННОЕ и счетчик второго списка увеличивается

 */
                    if (arrayList2.get(coubter2).equals(fromList1)) {
                        result.add(new LineItem(Type.ADDED, fromList2));
                        listTwoPoint++;
                        break;
                    }
                }
/*Если первым было найдено совпадение listTwoPoint с первым списком,
  то значение первого списка помечается , как УДАЛЕННОЕ и счетчик первого списка увеличивается

 */
                if (coubter1 < arrayList1.size()) {
                    if ((arrayList1.get(coubter1).equals(fromList2))) {
                        result.add(new LineItem(Type.REMOVED, fromList1));
                        listOnePoint++;
                        break;
                    }
                }


                coubter1++;
                coubter2++;
// Если списки перебрали, а совпадений так и не нашли, значит значение первого списка помечаем, как удаленное
// , а значение второго, как добаавленное и увеличиваем счетчики
                if ((coubter1 > arrayList1.size()) && (coubter2 > arrayList2.size())) {
                    result.add(new LineItem(Type.REMOVED, fromList1));
                    result.add(new LineItem(Type.ADDED, fromList2));
                    listOnePoint++;
                    listTwoPoint++;
                    break;

                }
            }
//********************************************************************************************
        }
        return result;
    }
}
